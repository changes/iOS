//
//  Building.swift
//  Changes
//
//  Created by Alex on 15/10/2016.
//  Copyright © 2016 Alex Steiner. All rights reserved.
//

import Foundation
import CoreLocation

public class Building : Dataset {
    public var images = Array<Image>()
    
    init(images: Array<Image>, coordinate: CLLocationCoordinate2D) {
        self.images = images
        super.init(coordinate: coordinate)
    }
    
    override init?(data: Dictionary<String, Any>) {
        guard let optional = data["data"] as? Dictionary<String, Any>,
            let images = optional["images"] as? Array<Dictionary<String, Any>>
            else { return nil }
        
        for image in images {
            if let year = image["year"] as? Int,
                let hash = image["hash"] as? String {
                self.images.append(Image(hash: hash, year: year))
            }
            
        }
        
        super.init(data: data)
    }
}

public class Image {
    public var image: UIImage?
    var hash: String
    public var year: Int
    
    init(hash: String, year: Int) {
        self.hash = hash
        self.year = year
    }
    
    public func fetchImage(completion:@escaping (_ image: UIImage?)->Void) {
        if let image = image {
            return completion(image)
        }
        DatasetAPI.requestImage(withURL: "/img?hash=\(hash)") { (image) in
            self.image = image
            completion(image)
        }
    }

}
